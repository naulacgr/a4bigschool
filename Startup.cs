﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(A4BSC.Startup))]
namespace A4BSC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
